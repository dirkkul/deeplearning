import numpy as np
def GetDecBound(X, Model):
    """Get the decision boundary between two classes, 
        https://stackoverflow.com/questions/22294241/plotting-a-decision-boundary-separating-2-classes-using-matplotlibs-pyplot """

    # create a mesh with stepsize h
    h = .02 
    x_min, x_max = X[0, :].min() - 0.5, X[0, :].max() + 0.5
    y_min, y_max = X[1, :].min() - 0.5, X[1, :].max() + 0.5
    xx, yy = np.meshgrid(np.arange(x_min, x_max, h),
                         np.arange(y_min, y_max, h))

    # Plot the decision boundary. For that, we will assign a color to each
    # point in the mesh [x_min, m_max]x[y_min, y_max].
    Z = Model.Predict(np.c_[xx.ravel(), yy.ravel()].T)

    # Put the result into a color plot
    Z = Z.reshape(xx.shape)
    return xx, yy, Z
