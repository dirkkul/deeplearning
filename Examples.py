import numpy as np
import sklearn.datasets
from model import Model

import matplotlib as mpl
mpl.use('Agg')
import matplotlib.pyplot as plt

from GetDecBound import GetDecBound

#get too example datasets
np.random.seed(50)
train_X, train_Y = sklearn.datasets.make_moons(n_samples=500, noise=.15)
Moon = (train_X.T, train_Y.reshape((1, train_Y.shape[0])), 25001, 'Moon')

train_X, train_Y = sklearn.datasets.make_circles(n_samples=500, factor=.5,
                                      noise=.05)
Circles = (train_X.T, train_Y.reshape((1, train_Y.shape[0])), 10001, 'Circles')

#Train a network for both examples and plot the result
for data in [Moon, Circles]:
    X, Y, Steps = data[0:3]

    layer = [X.shape[0], 22, 22, 1 ]
    model = Model(layer, seed = 5)
    cost = model.Train(X, Y, Steps, LR=0.1, print_cost=True)

    fig, (ax, ax2) = plt.subplots(2,1, figsize=(10,8))
    #plot the points and the decision boundary
    ax.set_ylabel('X1', fontsize = 20)
    ax.set_xlabel('X2', fontsize = 20)
    xx, yy, Z = GetDecBound(X, model)
    ax.contourf(xx, yy, Z, cmap=plt.cm.Spectral)
    ax.scatter(X[0,:], X[1,:], c=Y.reshape((Y.shape[1],)), s=40, cmap=plt.cm.Spectral)
    
    #plot the cost function
    ax2.set_ylabel('Cost', fontsize = 20)
    ax2.set_xlabel('Steps', fontsize = 20)
    ax2.set_xlim([0, Steps])
    ax2.set_ylim([0, 0.7])
    ax2.plot(np.arange(0, Steps, 100), cost, lw = 2)
    
    plt.tight_layout()
    plt.savefig(data[3]+'.pdf', bbox_inches='tight',dpi=500)
