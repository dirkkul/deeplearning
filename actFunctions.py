import numpy as np

def sigmoid(x):
    return 1. / (1. + np.exp(-x))

def relu(x):
    return np.maximum(x, 0)

"compute dZ for different activation functions"
def d_relu(Z):
    dZ = (Z > 0)
    assert (dZ.shape == Z.shape)
    return dZ
    
def d_sigmoid(Z):
    sm = sigmoid(Z)
    dZ = sm * (1. - sm)

    assert (dZ.shape == Z.shape)
    return dZ

