import numpy as np
import unittest

from actFunctions import sigmoid, relu
from actFunctions import d_relu, d_sigmoid

class MathTests(unittest.TestCase):
  
  def test_sigmoid(self):
    """ Test the sigmoid function, the numeric values are from wolfram alpha"""
    s1 = sigmoid(1)
    s025 = sigmoid(0.25)
    np.testing.assert_almost_equal(s1  , 0.7310585786)
    np.testing.assert_almost_equal(s025, 0.5621765008)
  
  def test_relu(self):
    """ relu(z) should return max(z,0) """
    re1 = relu(1)
    np.testing.assert_almost_equal(re1  , 1)
    reN1 = relu(-1)
    np.testing.assert_almost_equal(reN1  , 0)

  def test_d_relu(self):
    """ The derivative of the relu function is 1 for z>0 and 0 for z <= 0"""
    d_re1 = d_relu(np.ones((1,5)))
    np.testing.assert_equal(d_re1, np.ones((1,5)))
    d_re2 = d_relu(np.zeros((1,5)))
    np.testing.assert_equal(d_re2, np.zeros((1,5)))
    d_re3 = d_relu(np.array([-5, -1.1, -0., 0.5, 5]))
    np.testing.assert_equal(d_re3, np.array([0, 0 ,0 ,1, 1]))
    
  def test_d_sigmoid(self):
    """ Test derivative the sigmoid function is e^x/(e^x + 1)^2.
     numeric values are from wolfram alpha"""

    d_sm1 = d_sigmoid( np.array([1, 2, 0.5, -4]) )
    
    np.testing.assert_almost_equal(d_sm1[0]  , 0.1966119)
    np.testing.assert_almost_equal(d_sm1[1]  , 0.1049935)
    np.testing.assert_almost_equal(d_sm1[2]  , 0.2350037)
    np.testing.assert_almost_equal(d_sm1[3]  , 0.0176627)
    

if __name__ == "__main__":
    unittest.main()
