import numpy as np
import unittest

from model import Model
from actFunctions import sigmoid, relu


class ModelTests(unittest.TestCase):

    def setUp(self):
        """ create model with given setup and initialize random numbers"""
        np.random.seed(0)
        self.layer = [4, 5, 4, 1]
        self.Model = Model(self.layer)
        self.Model.LearningRate = 0.5

    def test_init(self):
        """ check if all parameters have been created correctly """

        # first, check the shape
        np.testing.assert_equal(self.Model.param["W1"].shape, (self.layer[1], self.layer[0]))
        np.testing.assert_equal(self.Model.param["W2"].shape, (self.layer[2], self.layer[1]))
        np.testing.assert_equal(self.Model.param["W3"].shape, (self.layer[3], self.layer[2]))

        np.testing.assert_equal(self.Model.param["b1"].shape, (self.layer[1], 1))
        np.testing.assert_equal(self.Model.param["b2"].shape, (self.layer[2], 1))
        np.testing.assert_equal(self.Model.param["b3"].shape, (self.layer[3], 1))

        # number of hidden layers
        np.testing.assert_equal(self.Model.param["L"], 3)

    def test_ForwardStep(self):
        """ Test one step in the forward propagation, for a single and multiple examples """

        # B is initialized to zero, so we change it here, to test it's effect
        self.Model.param["b1"] = np.arange(1, self.layer[1] + 1).reshape(self.layer[1], 1)

        for m in [1, 25]:  # number of examples
            X = np.ones((self.layer[0], m))

            A, Z = self.Model._ForwardStep(X, self.Model.param["W1"], self.Model.param["b1"], "relu")
            np.testing.assert_equal(Z, self.Model.param["W1"].dot(X) + self.Model.param["b1"])
            np.testing.assert_equal(relu(Z), A)

            # the first dim of the output A must be the number of neurons in layer[1]
            np.testing.assert_equal(A.shape[0], self.Model.param["b1"].shape[0])
            # We have one example, so the second dim must be one
            np.testing.assert_equal(A.shape[1], m)

            A, Z = self.Model._ForwardStep(X, self.Model.param["W1"], self.Model.param["b1"], "sigmoid")
            np.testing.assert_equal(sigmoid(Z), A)

    def test_ForwardPropagation(self):
        """ test the complete forward propagation through all L layers """

        # B is initialized to zero, so we change it here, to test it's effect
        self.Model.param["b1"] = np.arange(1, self.layer[1] + 1).reshape(self.layer[1], 1)
        self.Model.param["b2"] = np.arange(1, self.layer[2] + 1).reshape(self.layer[2], 1)
        self.Model.param["b3"] = np.arange(1, self.layer[3] + 1).reshape(self.layer[3], 1)

        for m in [1, 25]:  # number of examples
            X = np.ones((self.layer[0], m))

            # do all steps manually
            A1, Z1 = self.Model._ForwardStep(X, self.Model.param["W1"], self.Model.param["b1"], "relu")
            A2, Z2 = self.Model._ForwardStep(A1, self.Model.param["W2"], self.Model.param["b2"], "relu")
            A3, Z3 = self.Model._ForwardStep(A2, self.Model.param["W3"], self.Model.param["b3"], "sigmoid")

            # and now compare with the function
            AL, caches = self.Model._ForwardPropagation(X)
            np.testing.assert_equal(AL, A3)

    def test_costFunction(self):
        """ Test that the cost function returns sane results with several examples """
        m = 5
        X = np.random.randn(self.layer[0], m)
        Y = np.ones((1, m))

        AL, caches = self.Model._ForwardPropagation(X)
        cost = self.Model._CostFunction(AL, Y)

        # manually add cost up and compare it to the function return value
        costTest = 0
        for i in range(m):
            costTest += Y[0, i] * np.log(AL[0, i]) + (1-Y[0, i]) * np.log(1 - AL[0, i])

        costTest = -costTest/m
        np.testing.assert_almost_equal(costTest, cost)

        # check that cost goes down if more examples are correct
        AL[0, 0] = 0.999
        cost1 = self.Model._CostFunction(AL, Y)
        self.assertTrue(cost > cost1)

        AL[0, 1] = 0.999
        cost2 = self.Model._CostFunction(AL, Y)
        self.assertTrue(cost1 > cost2)

        # if every prediction is correct, the cost should be 0
        AL[0, :] = 0.99999999
        costAll = self.Model._CostFunction(AL, Y)
        self.assertTrue(cost2 > costAll)
        np.testing.assert_almost_equal(costAll, 0)

    def test_BackwardPropCost(self):
        """ The cost should be lower after we do one round.of forward and backward propagation """
        m = 1
        X = np.random.randn(self.layer[0], m)
        Y = np.zeros((1, m))
        AL, cache = self.Model._ForwardPropagation(X)
        cost1 = self.Model._CostFunction(AL, Y)
        grads = self.Model._BackwardPropagation(Y, cache, AL)
        self.Model._UpdateParam(grads)
        AL, cache = self.Model._ForwardPropagation(X)
        cost2 = self.Model._CostFunction(AL, Y)
        self.assertTrue(cost1 > cost2)

        # second round
        grads = self.Model._BackwardPropagation(Y, cache, AL)
        self.Model._UpdateParam(grads)
        AL, cache = self.Model._ForwardPropagation(X)
        cost3 = self.Model._CostFunction(AL, Y)
        self.assertTrue(cost2 > cost3)

    def test_train(self):
        """ test that the train function has the same output as doing the forward, backward cycle manually """
        m = 5
        X = np.random.randn(self.layer[0], m)
        Y = np.zeros((1, m))

        # we need to reset the parameters after learning
        paramSave = self.Model.param.copy()

        self.Model.Train(X, Y, 4, LR=0.5)
        paramTrain = self.Model.param.copy()

        # reset the parameters and do the loop manually
        self.Model.param = paramSave.copy()

        Model.LearningRate = 0.5
        AL, cache = self.Model._ForwardPropagation(X)
        grads = self.Model._BackwardPropagation(Y, cache, AL)
        self.Model._UpdateParam(grads)
        AL, cache = self.Model._ForwardPropagation(X)
        grads = self.Model._BackwardPropagation(Y, cache, AL)
        self.Model._UpdateParam(grads)
        AL, cache = self.Model._ForwardPropagation(X)
        grads = self.Model._BackwardPropagation(Y, cache, AL)
        self.Model._UpdateParam(grads)
        AL, cache = self.Model._ForwardPropagation(X)
        grads = self.Model._BackwardPropagation(Y, cache, AL)
        self.Model._UpdateParam(grads)
        for i in range(1, paramTrain["L"] + 1):
            np.testing.assert_almost_equal(paramTrain["W"+str(i)], self.Model.param["W"+str(i)])
            np.testing.assert_almost_equal(paramTrain["b"+str(i)], self.Model.param["b"+str(i)])

    def test_predict(self):
        """ Test that predict returns an array with the correct shape """
        m = 5
        X = np.random.randn(self.layer[0], m)
        Y = np.zeros((1, m))
        self.Model.Train(X, Y, 4, LR=0.5)
        self.assertEqual(self.Model.trained, True)

        m2 = 3
        X2 = np.random.randn(self.layer[0], m2)
        pred = self.Model.Predict(X2)
        self.assertEqual(pred.shape, (1, m2))

    def test_normalization(self):
        X = np.random.randn(self.layer[0], 50)
        self.Model.Normalize(X)

        from sklearn import preprocessing
        scaler = preprocessing.StandardScaler().fit(X.T)
        np.testing.assert_almost_equal(scaler.mean_ , self.Model.mean.T.reshape(self.Model.mean.T.shape[1], ))
        np.testing.assert_almost_equal(scaler.scale_, self.Model.std.T.reshape(self.Model.std.T.shape[1], ))

        X_scaled = self.Model._Normalize_Input(X)
        X_scaled_cor = preprocessing.scale(X.T)
        np.testing.assert_almost_equal(X_scaled, X_scaled_cor.T)

        X2 = np.random.randn(self.layer[0], 15)
        X_scaled = self.Model._Normalize_Input(X2)

if __name__ == "__main__":
    unittest.main()
