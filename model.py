import numpy as np
from actFunctions import sigmoid, relu
from actFunctions import d_relu, d_sigmoid

eps = np.finfo(float).eps
def SafeDivisor(x):
    x[np.abs(x) < eps] = eps
    return x


class Model:
    def __init__(self, Layers, seed=1, Xavier=False):
        """ This function initialises the parameters for the hidden layers of
            our neural network. The parameters are saved in a dictionary with
            names "Wx" and "bx", where x is the number of the hidden layer. The
            dimensions are:
            Wx.shape = (n[l], n[l-1])
            bx.shape = (n[l], 1).
            Here, n[l] is the number of units in the current layer and n[l-1] in
            the last one (we're getting our inputs from this layer).
        """

        # we're only doing binary classification
        assert (Layers[-1] == 1), 'Only binary classification is supported, the \
                                    last layer can only have one neuron'

        L = len(Layers)
        param = {}

        np.random.seed(seed)

        param["L"] = L - 1  # only hidden layers
        for i in range(1, L):
            if Xavier:  # use xavier initialization
                mult = np.sqrt(2./Layers[i-1])
            else:
                mult = 0.01
            param["W" + str(i)] = np.random.randn(Layers[i], Layers[i-1])*mult
            param["b" + str(i)] = np.zeros((Layers[i], 1))

        self.param = param

        self.normalized = False
        self.trained = False

    def Train(self, X, Y, Iter, LR=0.0075, print_cost=False):
        """ Train the model on given data """

        # check if input data has the correct form
        assert(X.shape[0] == self.param["W1"].shape[1]), "Input data has to be in columns"
        assert(Y.shape[0] == 1), "Input data has to be in columns"

        costs = []
        self.LearningRate = LR
        self.trained = True
        X = self._Normalize_Input(X)

        for iter in range(Iter):
            AL, cache = self._ForwardPropagation(X)
            grads = self._BackwardPropagation(Y, cache, AL)
            self._UpdateParam(grads)

            if iter % 100 == 0:
                cst = self._CostFunction(AL, Y)
                costs.append(cst)
                if print_cost:
                    print("cost at step " + str(iter), cst)
        return costs

    def Predict(self, X_pred):
        assert(self.trained)
        X_pred = self._Normalize_Input(X_pred)
        Al, _ = self._ForwardPropagation(X_pred)
        return (Al > 0.5) * np.ones_like(Al)

    def Normalize(self, X):
        """ We want our data to have zero mean and a std of 1. Therefore we compute mean and std
            for our training data and save it, to apply it to all input data."""
        self.normalized = True

        self.mean = X.mean(axis=1, keepdims=True)
        self.std = X.std(axis=1, keepdims=True)

    def _Normalize_Input(self, X):
        """ Normalize all input data to zero mean and a std of 1."""
        if self.normalized:
            X -= self.mean
            X /= self.std
        return X

    def _ForwardStep(self, A_last, W, b, NLactivation):
        """ This function does one forward step in our DNN. First it computes the linear step
            and then evaluates the nonlinear activation function.
        """
        Z = W.dot(A_last) + b
        if NLactivation == "relu":
            A = relu(Z)
        elif NLactivation == "sigmoid":
            A = sigmoid(Z)
        assert(A.shape == Z.shape)
        return A, Z

    def _ForwardPropagation(self, X):
        """ Do the complete forward propagation for all L layers. We save important
            intermediate data (Z and A) in the cache to use them in the backward step.
            The first L-1 steps are with relu and the last with the sigmoid for classification. """
        caches = []
        A_prev = X

        for l in range(1, self.param["L"]):
            A, Z = self._ForwardStep(A_prev, self.param["W" + str(l)], self.param["b" + str(l)], "relu")
            caches.append([Z, A_prev])
            A_prev = A

        # Last step is sigmoid
        AL, Z = self._ForwardStep(A_prev, self.param["W" + str(self.param["L"])], self.param["b" + str(self.param["L"])], "sigmoid")
        caches.append([Z, A])

        return AL, caches

    def _CostFunction(self, AL, Y):
        """ Compute the cost of the current prediction for all examples"""
        m = AL.shape[1]

        # Cost function for binary classification, make sure to not divide by zero
        cost = -1/m * np.sum(Y * np.log(SafeDivisor(AL)) + (1 - Y) * np.log(SafeDivisor(1 - AL)))
        cost = np.squeeze(cost)
        return cost

    def _BackwardStep(self, W, dA, cache, NLactivation):
        """ This function does a step backward and computes all the gradients we need for gradient descent"""
        Z, A_prev = cache
        if NLactivation == "relu":
            dZ = d_relu(Z) * dA
        elif NLactivation == "sigmoid":
            dZ = d_sigmoid(Z) * dA

        m = A_prev.shape[1]
        dW = 1/m * dZ.dot(A_prev.T)
        db = 1/m * np.sum(dZ, axis=1, keepdims=True)
        dA_prev = W.T.dot(dZ)

        assert (dA_prev.shape == A_prev.shape)
        assert (dW.shape == W.shape)

        return(dA_prev, dW, db)

    def _BackwardPropagation(self, Y, cache, AL):
        """ Loop backwards through all layers, to compute the gradients for gradient descent."""
        grads = {}
        # initialize the back propagation, make sure to not divide by zero
        dAL = - (np.divide(Y, SafeDivisor(AL)) - np.divide(1 - Y, SafeDivisor(1 - AL)))

        L = self.param["L"]
        grads["dA"+str(L-1)], grads["dW"+str(L)], grads["db"+str(L)] = self._BackwardStep(self.param["W"+str(L)], dAL, cache[-1], "sigmoid")
        # from L-2 to 1
        for i in reversed(range(L-1)):
            dA_prev, grads["dW"+str(i+1)], grads["db"+str(i+1)] = self._BackwardStep(self.param["W"+str(i+1)],
                                                                               grads["dA" + str(i+1)], cache[i], "relu")
            grads["dA"+str(i)] = dA_prev

        return grads

    def _UpdateParam(self, grad):
        """ Update all parameters using gradient descent"""
        for i in range(1, self.param["L"] + 1):
            self.param["W" + str(i)] = self.param["W" + str(i)] - self.LearningRate*grad["dW" + str(i)]
            self.param["b" + str(i)] = self.param["b" + str(i)] - self.LearningRate*grad["db" + str(i)]
