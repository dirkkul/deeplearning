"""
    This file compares the model output with the output of Andrew Ngs notebooks from his coursera course.
"""

import numpy as np
import unittest
import sklearn.datasets

from model import Model
from actFunctions import sigmoid, relu

from testData import load_AL, loadGrad, load_ParamTrain


class ModelTests(unittest.TestCase):
    
    def setUp(self):
        """ create model with given setup and initialize random numbers"""
        
        #Simple, shallow model
        layerShallow = [3 , 2, 1]
        self.ModelShallow = Model(layerShallow, seed = 1)
        self.ModelShallow.LearningRate = 0.1
        self.ModelShallow.Layer = layerShallow
        
        #test a more complex model and generate a dataset for it
        np.random.seed(3)
        train_X, train_Y = sklearn.datasets.make_moons(n_samples=300, noise=.2)
        self.X = train_X.T
        self.Y = train_Y.reshape((1, train_Y.shape[0]))
            
        layerDeep = [self.X.shape[0], 15, 15, 1 ]
        self.ModelDeep = Model(layerDeep, seed = 1)
        self.ModelDeep.LearningRate = 0.1
        self.ModelDeep.Layer = layerDeep

    def test_init_ShallowValues(self):
        """ check that the initialized parameters agree with Andre Ng's"""
        
        Ng_W1 = np.array([[ 0.01624345, -0.00611756, -0.00528172], [-0.01072969, 0.00865408, -0.02301539 ]])
        Ng_W2 = np.array([[ 0.01744812, -0.00761207]])
        
        np.testing.assert_almost_equal(self.ModelShallow.param["W1"], Ng_W1)
        np.testing.assert_almost_equal(self.ModelShallow.param["W2"], Ng_W2)
        np.testing.assert_almost_equal(self.ModelShallow.param["b1"], np.zeros_like(self.ModelShallow.param["b1"]))
        np.testing.assert_almost_equal(self.ModelShallow.param["b2"], np.zeros_like(self.ModelShallow.param["b2"]))
        
    def test_ForwardStepShallow_Values(self):
        """ Do one forward step and check that the linear and the nonlinear output is correct"""
        
        np.random.seed(1)
        X = np.random.randn(3,2)
        A1_s, Z1 = self.ModelShallow._ForwardStep(X, self.ModelShallow.param["W1"], self.ModelShallow.param["b1"], "sigmoid")
        A1_r, Z1 = self.ModelShallow._ForwardStep(X, self.ModelShallow.param["W1"], self.ModelShallow.param["b1"], "relu")
        
        #test linear forward step
        Ng_Z = [[ 0.02504526, 0.00878299], [-0.04191725,  0.05024921]]
        np.testing.assert_almost_equal(Z1, Ng_Z)
        
        #test NL forward step
        Ng_Zs = [[0.50626099, 0.50219573], [0.48952222, 0.51255966]]
        np.testing.assert_almost_equal(A1_s, Ng_Zs)
        Ng_Zr = [[0.02504526, 0.00878299], [0., 0.05024921]]
        np.testing.assert_almost_equal(A1_r, Ng_Zr)
    
    def test_ForwardPropagationShallow(self):
        """ Complete forward propagation for the shallow network"""
        np.random.seed(1)
        X = np.random.randn(3,2)
        AL, _ = self.ModelShallow._ForwardPropagation(X)
        
        Ng_Al = [[0.50010925, 0.49994269]]
        np.testing.assert_almost_equal(AL, Ng_Al)
    
    def test_ForwardPropagationDeep(self):
        """ Complete forward propagation for the deep network"""
        AL, _ = self.ModelDeep._ForwardPropagation(self.X)
        
        Ng_Al = load_AL()
        np.testing.assert_almost_equal(AL, Ng_Al)
        
    def test_CostFunction(self):
        """Check that the cost function returns the correct result"""

        Y = np.asarray([[1, 1, 1, 0, 0, 1, 0, 1, 0, 0, 0 ,0 ,1]])
        aL = np.array([[.8,.9,.4,.2,.9,.6,.2,.3,.5,.4,.9,.1,.4]])
        
        cost = self.ModelShallow._CostFunction(aL, Y)
        
        cost_Ng = 0.7874365051855805
        np.testing.assert_almost_equal(cost, cost_Ng)
        
    def test_BackwardStep(self):
        """Do one backward step and check the gradients for the shallow network"""
        np.random.seed(1)
        X = np.random.randn(3,2)
        Y = np.asarray([[1, 1]])
        AL, cache = self.ModelShallow._ForwardPropagation(X)
        
        dAL = - (np.divide(Y, AL) - np.divide(1 - Y, 1 - AL))
        dA_prev, dW, db = self.ModelShallow._BackwardStep(self.ModelShallow.param["W2"], dAL, cache[-1], "relu")
        
        Ng_dA_prev_re = [[-0.03488861, 0.        ], [ 0.01522081, 0.]]
        Ng_dW_re = [[-0.02503979,  0.        ]]
        Ng_db_re = [[-0.99978155]]
        np.testing.assert_almost_equal(dA_prev, Ng_dA_prev_re)
        np.testing.assert_almost_equal(dW, Ng_dW_re)
        np.testing.assert_almost_equal(db, Ng_db_re)
        
        dA_prev, dW, db = self.ModelShallow._BackwardStep(self.ModelShallow.param["W2"], dAL, cache[-1], "sigmoid")
        
        Ng_dA_prev_s = [[-0.00872215, -0.00872506], [ 0.0038052, 0.00380647]]
        Ng_dW_s = [[-0.00845595, -0.01256374]]
        Ng_db_s = [[-0.49997403]]
        
        np.testing.assert_almost_equal(dA_prev, Ng_dA_prev_s)
        np.testing.assert_almost_equal(dW, Ng_dW_s)
        np.testing.assert_almost_equal(db, Ng_db_s)

    def test_BackwardPropagationShallow(self):
        """Complete backward step for the shallow network"""
        np.random.seed(1)
        X = np.random.randn(3,2)
        Y = np.asarray([[1, 1]])
        AL, cache = self.ModelShallow._ForwardPropagation(X)

        grads = self.ModelShallow._BackwardPropagation(Y, cache, AL)
        
        Ng_dW1 = [[-0.00441509, 0.00698425, 0.00626642], [-0.00116432, -0.00204211, -0.00438037]]
        Ng_db1 = [[-0.00872361], [ 0.00190324]]
        Ng_dA1 = [[-0.00872215, -0.00872506], [ 0.0038052, 0.00380647]]
        Ng_dW2 = [[-0.00845595, -0.01256374]]
        Ng_db2 = [[-0.49997403]]
        np.testing.assert_almost_equal(grads["dW2"], Ng_dW2)
        np.testing.assert_almost_equal(grads["db2"], Ng_db2)
        np.testing.assert_almost_equal(grads["dW1"], Ng_dW1)
        np.testing.assert_almost_equal(grads["db1"], Ng_db1)
        np.testing.assert_almost_equal(grads["dA1"], Ng_dA1)

    def test_BackwardPropagationDeep(self):
        """Complete backward step for the deep network"""
        AL, cache = self.ModelDeep._ForwardPropagation(self.X)
        grads = self.ModelDeep._BackwardPropagation(self.Y, cache, AL)
        
        Ng_grads = loadGrad()
        np.testing.assert_almost_equal(grads["dW3"], Ng_grads["dW3"])
        np.testing.assert_almost_equal(grads["db3"], Ng_grads["db3"])
        np.testing.assert_almost_equal(grads["dW2"], Ng_grads["dW2"])
        np.testing.assert_almost_equal(grads["db2"], Ng_grads["db2"])
        np.testing.assert_almost_equal(grads["dW1"], Ng_grads["dW1"])
        np.testing.assert_almost_equal(grads["db1"], Ng_grads["db1"])
        np.testing.assert_almost_equal(grads["dA2"], Ng_grads["dA2"])
        np.testing.assert_almost_equal(grads["dA1"], Ng_grads["dA1"])
    
    def test_TestDeepModelTraining(self):
        """ Train the network for 10 steps and check the updated parameters"""
        SaveParam = self.ModelShallow.param.copy()
        for i in range(10):
            AL, cache = self.ModelDeep._ForwardPropagation(self.X)
            grads = self.ModelDeep._BackwardPropagation(self.Y, cache, AL)
            self.ModelDeep._UpdateParam(grads)
            
        Ng_param = load_ParamTrain()
        for key in Ng_param.items():
            np.testing.assert_almost_equal(self.ModelDeep.param[key[0]], Ng_param[key[0]])
        
        self.ModelShallow.param = SaveParam

    def test_UpdateFunction(self):
        """ Update the parameters after training """
        np.random.seed(1)
        X = np.random.randn(3,2)
        Y = np.asarray([[1, 1]])
        
        SaveParam = self.ModelShallow.param.copy()
        
        AL, cache = self.ModelShallow._ForwardPropagation(X)
        grads = self.ModelShallow._BackwardPropagation(Y, cache, AL)
        
        
        self.ModelShallow._UpdateParam(grads)
       
        Ng_W1 = [[ 0.01668496, -0.00681599, -0.00590836], [-0.01061325, 0.00885829, -0.02257735]]
        Ng_b1 = [[ 0.00087236], [-0.00019032]]
        Ng_W2 = [[ 0.01829371, -0.00635569]]
        Ng_b2 = [[0.0499974]]
        
        np.testing.assert_almost_equal(self.ModelShallow.param["W1"], Ng_W1)
        np.testing.assert_almost_equal(self.ModelShallow.param["b1"], Ng_b1)
        np.testing.assert_almost_equal(self.ModelShallow.param["W2"], Ng_W2)
        np.testing.assert_almost_equal(self.ModelShallow.param["b2"], Ng_b2)
        
        self.ModelShallow.param = SaveParam
        

if __name__ == "__main__":
    unittest.main()
