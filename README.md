# Introduction

This repository implements a deep learning algorithm using `numpy`. This is a toy model to better understand how deep/machine learning works and should not be used for anything important. Most things here are based on the excellent coursera course by [Andrew NG](https://www.coursera.org/learn/neural-networks-deep-learning).

We want to analyze a dataset with the features $`X`$ and the target $`Y`$. The goal is to create a model that can correctly classify new instances, where we only know the input features. For this, we create a network with $`L`$ layers, where each can have variing amounts of neurons.

For simplicity, this repository will only include binary classification

# Examples and usage
You can initialize the model with `model = Model([4,3,2,1], seed = 5, Xavier = True)`.  
Afterwards, training can be done with `cost = model.Train(X, Y, Steps, LR=0.01, print_cost=True)`.  
Here, the input needs to have the shapes `X = np.random.randn(layer[0], m)` and `Y = np.zeros((1, m) )`, where `m` is the number of examples.
The training function returns a list with the cost evolution.  
Predictions can be made with `prediction = model.Predict(X_test)`.

The file `Examples.py` contains two examples with auto-generated datasets. A trained model can be seen in the screenshot below.

<img src="Image_Moon.png" alt="Example Image" style="width:180px;"/>

# Technical Details

## Forward propagation

Each layer $`i`$ in our network with $`L`$ layers consists of two distinct steps. First, a linear step $`Z^i = W^i\cdot A^{i-1} + b^i`$, where $`W^i`$ and $`b^i`$ are the parameters of the given layer and $`A^{i-1}`$ is the output of the last layer (or the input $`X`$). Second, a non-linear step $`A^i = \sigma(Z^i)`$, where $`\sigma`$ is a non-linear activation function. Common choices for $`\sigma`$ are the `Relu` or the sigmoid function $`S(x) = \frac{1}{1+e^{-x}}`$. The output of the last layer $`A^L`$ is the prediction of the current network.

## Cost function

To quantify the quality of the prediction of our model, we introduce the cost $`J(W,B) = \frac{1}{m} \sum_j^m L_j`$ with the loss $`L`$ that measures the distance between our prediction and target for each of our $`m`$ examples.
Depending on the task, we utilize different loss functions, a common choice for regression is $` L = |Y - A^L|`$ and for classification $` L = Y\log(A^L) + (1-Y)\log(1-A^L)`$.

## Backward Propagation using gradient descent
In the next step, we want to improve the predictions of our network. As we measure the quality of our prediction with the cost $`J`$ we want to find parameters $`W, B`$ with lower cost. One way to do this is to calculate the gradient of the cost with respect to the parameters and then update the parameters downwards the direction with the highest gradient. Therefore, we need to compute $`\partial_{w/b} J = \frac{1}{m} \sum_j^m \partial_{w/b} L_j`$.

Using the chain rule, we break up $`\partial_{W/B} J`$ into derivatives that we can directly compute:  
$`\partial_W L = \frac{\partial L}{\partial W} = \frac{\partial L}{\partial A}\frac{\partial A}{\partial W} =
\frac{\partial L}{\partial A}\frac{\partial A}{\partial Z} \frac{\partial Z}{\partial W}`$  
and  
$` \partial_B L = \frac{\partial L}{\partial A}\frac{\partial A}{\partial Z} \frac{\partial Z}{\partial B} `$.

Now, we can compute each of these derivatives independently:

-  $`\frac{\partial L}{\partial A}`$. For the output layer this is the derivative of the loss function, e.g. with classification $`\frac{\partial L}{\partial A} = -Y/A + \frac{1-Y}{1-A}`$. If we're in a hidden layer, this derivative evaluates to $`\frac{\partial L}{\partial A} = W^{T +1} \cdot (\frac{\partial L}{\partial A}\frac{\partial A}{\partial Z})^{+1}`$, where the ^{+1} marks that they belong to the layer above.

-  $`\frac{\partial a}{\partial Z} = \partial_Z \sigma(Z)`$, e.g. the derivative of the activation function. For the sigmoid function $`\sigma(Z)' = \sigma(Z)(1-\sigma(Z)) = A(1-A)`$.

-  $`\frac{\partial Z}{\partial W} = \partial_W (W\cdot A^{-1} + B) = A^{-1}`$, here ^{-1} indicates that A is the output of the previous layer.
-  $`\frac{\partial Z}{\partial B} = \partial_B (W\cdot A^{-1} + B) = 1`$.

For the outer layer we therefore get $`\partial_W L = (A - 1)\cdot A^{-1}`$ and $`\partial_B L = (A - 1)`$. The inner layers appear more complicated with $`\partial_W L = \partial_Z L\cdot A^{-1}`$ and $`\partial_B L = \partial_Z L`$. However, we computed $`\partial_Z L`$ in the layer above and just need to pass this result down. Now, we just need to average the loss derivatives to compute the total cost derivatives.

With these results, we can update our parameters with $`W = W - \alpha \partial_W J`$ and $`B = B - \alpha \partial_B J`$ with the learning rate $`\alpha`$. The update is proportional to the steepness of the cost's gradient.

## Notation
I use the following notations
-  $`\frac{\partial Z}{\partial W} \equiv \partial_W Z`$
-  .^T implies a transposed matrix
-  If all terms in an equation are at the same layer, I sometimes omit the layer indices
-  .^{+1/-1} means that the given term is from the layer above/below